<?php

namespace Database\Seeders;

use DateTime;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JobSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $skills = [
            [
                'name' => 'Frontend Developer',
                'created_at' => new DateTime(),
                'updated_at' => null
            ],
            [
                'name' => 'Backend Developer',
                'created_at' => new DateTime,
                'updated_at' => null
            ],
            [
                'name' => 'Fullstack Developer',
                'created_at' => new DateTime,
                'updated_at' => null
            ],
        ];

        DB::table('jobs')->insert($skills);
    }
}

<?php

namespace Database\Seeders;

use DateTime;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;

class SkillSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $skills = [
            [
                'name' => 'Javascript',
                'created_at' => new DateTime,
                'updated_at' => null
            ],
            [
                'name' => 'Laravel',
                'created_at' => new DateTime,
                'updated_at' => null
            ],
            [
                'name' => 'PHP',
                'created_at' => new DateTime,
                'updated_at' => null
            ],
        ];

        DB::table('skills')->insert($skills);
    }
}

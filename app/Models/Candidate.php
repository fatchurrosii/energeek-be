<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Wildside\Userstamps\Userstamps;

class Candidate extends Model
{
    use HasFactory;
    use Userstamps;

    protected $date = [
        'updated_at',
        'created_at',
        'deleted_at',
        'created_by',
        'updated_by',
        'deleted_by'

    ];
    protected $guarded = [''];

    public function jobs()
    {
        return $this->belongsTo(Job::class, 'job_id');
    }
    public function skills()
    {
        return $this->BelongsToMany(Skill::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Wildside\Userstamps\Userstamps;

class Skill extends Model
{
    use HasFactory;
    use Userstamps;

    protected $date = [
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    public function candidates()
    {
        return $this->belongsToMany(Candidate::class);
    }
}
